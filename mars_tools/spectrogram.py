#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Plot spectrograms in two different frequency channels

:copyright:
    Simon Stähler (mail@simonstaehler.com), 2018
:license:
    None
'''

import numpy as np
import obspy
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from obspy.signal.util import next_pow_2
from obspy.signal.tf_misfit import cwt
from obspy import UTCDateTime as utct
from mars_tools.insight_time import solify

import argparse
plt.rcParams['agg.path.chunksize'] = 1000


def plot_cwf(tr, fmin=1./50, fmax=1./2, w0=8):
    npts = tr.stats.npts
    dt = tr.stats.delta

    scalogram = abs(cwt(tr.data, dt, w0=w0, nf=200,
                        fmin=fmin, fmax=fmax))

    t = np.linspace(0, dt * npts, npts)
    f = np.logspace(np.log10(fmin),
                    np.log10(fmax),
                    scalogram.shape[0])
    return scalogram**2, f, t


def calc_specgram_dual(st_LF, st_HF, st_press,
                       winlen_sec_LF=1800,
                       winlen_sec_HF=10.0,
                       fmax=None,
                       overlap=0.5, kind='spec',
                       vmin=None, vmax=None, log=True,
                       fnam=None, noise=None):
    """

    :type st_press: obspy.Stream
    """
    # Some input checking
    freq_HF = st_HF[0].stats.sampling_rate
    if freq_HF <= 2.0:
        raise KeyError('Sampling rate of HF data must be > 2sps, is %4.1fsps' %
                       freq_HF)
    winlen_LF = int(winlen_sec_LF * st_LF[0].stats.sampling_rate)
    winlen_HF = int(winlen_sec_HF * st_HF[0].stats.sampling_rate)
    winlen_press = int(900. * st_press[0].stats.sampling_rate)

    fmin_LF = 2. / winlen_sec_LF
    fmax_LF = 1.0

    fmin_HF = 1.0
    if fmax is None:
        fmax_HF = freq_HF / 2.
    else:
        fmax_HF = fmax

    fmin_press = 1./500. # 2. / winlen_press
    fmax_press = 1. # st_press[0].stats.sampling_rate / 2.
    vmin_p = -60
    vmax_p = -30
    # st_LF.differentiate()
    st_LF.filter('bandpass', freqmin=fmin_LF*0.9, freqmax=fmax_LF*1.2,
                 zerophase=True, corners=6)
    st_HF.filter('bandpass', freqmin=fmin_HF*0.8, freqmax=fmax_HF,
                 zerophase=True, corners=6)

    if st_press[0].stats.location == '02':
        for tr in st_press:
            tr.data = tr.data * 3.37e-5
    st_press.differentiate()
    st_press.detrend()
    st_press.filter('highpass', freq=fmin_press, zerophase=True, corners=6)

    ax_cb, ax_psd_HF, ax_psd_LF, ax_psd_press, \
        ax_seis_HF, ax_seis_LF, ax_seis_press, \
        ax_spec_HF, ax_spec_LF, ax_spec_press = create_axes()

    for tr in st_LF:
        t0 = float(tr.stats.starttime)
        ax_seis_LF.plot((tr.times() + t0), tr.data*1e9,
                        'navy', lw=0.3)
    for tr in st_HF:
        t0 = float(tr.stats.starttime)
        ax_seis_HF.plot((tr.times() + t0), tr.data*1e9,
                        'darkred', lw=0.3)

    for tr in st_press:
        t0 = float(tr.stats.starttime)
        ax_seis_press.plot((tr.times() + t0), tr.data,
                           'darkgreen', lw=0.3)

    for st, ax_spec, ax_psd, flim, winlen, cmap, vlim in \
            zip([st_LF, st_HF, st_press],
                [ax_spec_LF, ax_spec_HF, ax_spec_press],
                [ax_psd_LF, ax_psd_HF, ax_psd_press],
                [(fmin_LF, fmax_LF),
                 (fmin_HF, fmax_HF),
                 (fmin_press, fmax_press)],
                [winlen_LF, winlen_HF, winlen_press],
                ['plasma', 'plasma', 'viridis'],
                [(vmin, vmax), (vmin, vmax), (vmin_p, vmax_p)]):

        for tr in st:
            if (kind == 'cwt' and winlen < 20. * tr.stats.sampling_rate) or \
                    (kind == 'spec' and winlen < 600 * tr.stats.sampling_rate):
                p, f, t = mlab.specgram(tr.data, NFFT=winlen,
                                        Fs=tr.stats.sampling_rate,
                                        noverlap=int(winlen*overlap),
                                        pad_to=next_pow_2(winlen)*4)
            else:
                p, f, t = plot_cwf(tr, w0=16,
                                   fmin=flim[0], fmax=flim[1])

            t += float(tr.stats.starttime)

            bol = np.array((f > flim[0], f < flim[1])).all(axis=0)

            vmin = vlim[0]
            vmax = vlim[1]
            if vmin is None:
                vmin = np.percentile(10 * np.log10(p[bol, :]), q=1, axis=None)
            if vmax is None:
                vmax = np.percentile(10 * np.log10(p[bol, :]), q=90, axis=None)

            if log:
                ax_spec.pcolormesh(t, f[bol], 10*np.log10(p[bol, :]),
                                   vmin=vmin,
                                   vmax=vmax,
                                   cmap=cmap)
            else:
                ax_spec.pcolormesh(t, f[bol], p[bol, :],
                                   vmin=vmin, vmax=vmax,
                                   cmap=cmap)

            plot_psd(ax_psd, bol, f, p)

        ax_psd.set_xlim(vmin, vmax)

        if noise == 'Earth':
            plt_earth_noise(ax_psd)
        elif noise == 'Mars':
            raise ValueError('Mars noise needs external library, deactivated')

    set_axis(ax_cb, ax_psd_HF, ax_psd_LF, ax_psd_press, ax_seis_HF, ax_seis_LF,
             ax_seis_press, ax_spec_HF, ax_spec_LF, ax_spec_press, fmax_HF,
             fmax_LF, fmax_press, fmin_HF, fmin_LF, fmin_press, st_HF, st_LF,
             st_press)

    if fnam:
        plt.savefig(fnam, dpi=240)
        plt.close()
    else:
        plt.show()


def plt_earth_noise(ax_psd):
    nhnm = obspy.signal.spectral_estimation.get_nhnm()
    nlnm = obspy.signal.spectral_estimation.get_nlnm()
    ix = nlnm[1]
    iy = 1. / nlnm[0]
    verts = [(-300, 1. / nlnm[0][0])] + list(zip(ix, iy)) + [
        (-300, 1. / nlnm[0][-1])]
    poly = Polygon(verts, facecolor='0.9', edgecolor='0.5')
    ax_psd.add_patch(poly)
    ix = nhnm[1]
    iy = 1. / nhnm[0]
    verts = [(0, 1. / nhnm[0][0])] + list(zip(ix, iy)) + [(0, 1. / nhnm[0][-1])]
    poly = Polygon(verts, facecolor='0.9', edgecolor='0.5')
    ax_psd.add_patch(poly)
    ax_psd.plot(nhnm[1], 1. / nhnm[0], color='darkgrey', linestyle='dashed')
    ax_psd.plot(nlnm[1], 1. / nlnm[0], color='darkgrey', linestyle='dashed')


def plot_psd(ax_psd, bol, f, p):
    median = np.percentile(p[bol, :], axis=1, q=50)
    perc_95 = np.percentile(p[bol, :], axis=1, q=95)
    perc_05 = np.percentile(p[bol, :], axis=1, q=5)
    perc_99 = np.percentile(p[bol, :], axis=1, q=99)
    perc_01 = np.percentile(p[bol, :], axis=1, q=1)
    ax_psd.plot(10 * np.log10(median), f[bol],
                color='indigo')
    ax_psd.plot(10 * np.log10(perc_95), f[bol],
                color='darkgrey', linestyle='dashed')
    ax_psd.plot(10 * np.log10(perc_05), f[bol],
                color='darkgrey', linestyle='dashed')
    ax_psd.plot(10 * np.log10(perc_99), f[bol],
                color='indigo', linestyle='dotted')
    ax_psd.plot(10 * np.log10(perc_01), f[bol],
                color='indigo', linestyle='dotted')


def set_axis(ax_cb, ax_psd_HF, ax_psd_LF, ax_psd_press, ax_seis_HF, ax_seis_LF,
             ax_seis_press, ax_spec_HF, ax_spec_LF, ax_spec_press, fmax_HF,
             fmax_LF, fmax_press, fmin_HF, fmin_LF, fmin_press, st_HF, st_LF,
             st_press):

    tstart = min(st_LF[0].stats.starttime,
                 st_HF[0].stats.starttime,
                 st_press[0].stats.starttime)
    tend = min(st_LF[0].stats.endtime,
               st_HF[0].stats.endtime,
               st_press[0].stats.endtime)
    ax_seis_LF.set_ylim(st_LF[0].std() * 1e10 * np.asarray([-3., 1.]))
    ax_seis_HF.set_ylim(st_HF[0].std() * 1e10 * np.asarray([-2., 2.]))
    ax_seis_press.set_ylim(st_press[0].std() * 10. * np.asarray([-1., 3.]))
    ax_seis_LF.set_ylabel('%s [nm/s²] \n< 1 Hz' % st_LF[0].stats.channel,
                          color='navy')
    ax_seis_HF.set_ylabel('%s [nm/s] \n> 1 Hz' % st_HF[0].stats.channel,
                          color='darkred')
    ax_seis_press.set_ylabel('%s [Pa/s]' % st_press[0].stats.channel,
                             color='darkgreen')
    ax_seis_LF.tick_params('y', colors='navy')
    ax_seis_HF.tick_params('y', colors='darkred')
    ax_spec_HF.set_ylim(fmin_HF, fmax_HF)
    ax_spec_HF.set_ylabel('frequency / Hz', fontsize=12)
    for ax_spec in [ax_spec_press, ax_spec_LF]:
        ax_spec.set_yscale('log')
        ax_spec.set_ylabel('period / seconds', fontsize=12)
    __dayplot_set_x_ticks(ax_spec_LF,
                          tstart,  # st_LF[0].stats.starttime,
                          tend)    #st_LF[-1].stats.endtime)
    for ax_press in [ax_spec_press, ax_psd_press]:
        ax_press.xaxis.set_label_position('top')
        ax_press.xaxis.set_ticks_position('top')
    ax_psd_press.set_xlabel('PSD (Pa/s)²/Hz')
    # This needs to be done for both axes, otherwise the PSD and the Spec axis
    # plot weird yticks on top of each other
    for ax in [ax_spec_LF, ax_psd_LF]:
        ax.set_ylim(fmin_LF, fmax_LF)
        tickvals = np.asarray(
            [1. / 2, 1. / 5, 1. / 10, 1. / 20, 1. / 50, 1. / 100,
             1. / 200., 1. / 500, 1. / 1000.])
        ax.set_yticks(tickvals[tickvals >= fmin_LF])
        ax.set_yticklabels(['2', '5', '10', '20', '50', '100', '200', '500',
                            '1000'])
        ax.set_yticklabels([], minor=True)
    for ax in [ax_spec_press, ax_psd_press]:
        ax.set_ylim(fmax_press, fmin_press)
        tickvals = np.asarray(
            [1. / 2, 1. / 5, 1. / 10, 1. / 20, 1. / 50, 1. / 100,
             1. / 200., 1. / 500, 1. / 1000.])
        ax.set_yticks(tickvals[tickvals >= fmin_press])
        ax.set_yticklabels(['2', '5', '10', '20', '50', '100', '200', '500',
                            '1000'])
        ax.set_yticklabels([], minor=True)
    for ax_psd in [ax_psd_HF, ax_psd_LF, ax_psd_press]:
        ax_psd.yaxis.set_label_position("right")
        ax_psd.yaxis.set_ticks_position("right")
    ax_psd_LF.set_xlabel('PSD (m/s²)²/Hz [dB]')
    ax_psd_LF.set_ylabel('period / seconds', fontsize=12)
    ax_psd_HF.set_ylabel('frequency / Hz', fontsize=12)
    ax_psd_HF.set_xticks([])
    # make unnecessary labels disappear
    for ax in [ax_spec_HF, ax_seis_LF]:
        plt.setp(ax.get_xticklabels(), visible=False)
    # Axis with colorbar
    mappable = ax_spec_HF.collections[0]
    plt.colorbar(mappable=mappable, cax=ax_cb)
    ax_cb.set_ylabel('PSD (m/s)²/Hz')
    # Axis with Martian time
    ax_LMST = ax_spec_LF.twiny()
    # Move twinned axis ticks and label from top to bottom
    ax_LMST.xaxis.set_ticks_position("bottom")
    ax_LMST.xaxis.set_label_position("bottom")
    # Offset the twin axis below the host
    ax_LMST.spines["bottom"].set_position(("axes", -0.15))
    # Turn on the frame for the twin axis, but then hide all
    # but the bottom spine
    ax_LMST.set_frame_on(True)
    ax_LMST.patch.set_visible(False)
    for sp in ax_LMST.spines.values():
        sp.set_visible(False)
    ax_LMST.spines["bottom"].set_visible(True)
    ax_LMST.set_xlabel(r"LMST")
    __dayplot_set_x_ticks(ax_LMST,
                          solify(st_LF[0].stats.starttime),
                          solify(st_LF[-1].stats.endtime),
                          sol=True)


def create_axes():
    fig = plt.figure(figsize=(16, 9))
    # [left bottom width height]
    h_base = 0.13
    h_spec_LF = 0.3  # 0.4
    h_spec_HF = 0.2  # 0.25
    h_seis = 0.15  # 0.2
    h_spec_press = 0.17  # 0.25
    w_base = 0.06
    w_spec = 0.8
    w_psd = 0.1
    ax_seis_LF = fig.add_axes([w_base, h_base + h_spec_LF + h_spec_HF,
                               w_spec, h_seis],
                              label='seismogram LF')
    ax_seis_HF = ax_seis_LF.twinx()
    ax_seis_press = ax_seis_LF.twinx()
    ax_spec_LF = fig.add_axes([w_base, h_base, w_spec, h_spec_LF],
                              sharex=ax_seis_LF,
                              label='spectrogram LF')
    ax_spec_HF = fig.add_axes([w_base, h_base + h_spec_LF, w_spec, h_spec_HF],
                              sharex=ax_seis_LF,
                              label='spectrogram HF')
    ax_spec_press = fig.add_axes([w_base,
                                  h_base + h_spec_LF + h_spec_HF + h_seis,
                                  w_spec,
                                  h_spec_press],
                                 sharex=ax_seis_LF,
                                 label='spectrogram pressure')
    ax_psd_LF = fig.add_axes([w_base + w_spec, h_base, w_psd, h_spec_LF],
                             sharey=ax_spec_LF,
                             label='PSD LF')
    ax_psd_HF = fig.add_axes([w_base + w_spec, h_base + h_spec_LF,
                              0.1, h_spec_HF],
                             sharey=ax_spec_HF,
                             label='PSD HF')
    ax_psd_press = fig.add_axes([w_base + w_spec,
                                 h_base + h_spec_LF + h_spec_HF + h_seis,
                                 0.1, h_spec_press],
                                sharey=ax_spec_press,
                                label='PSD pressure')
    # Colorbar axis
    ax_cb = fig.add_axes([w_base + w_spec + w_psd / 2.,
                          h_base + h_spec_HF + h_spec_LF + h_seis * 0.1,
                          w_psd * 0.2,
                          h_seis * 0.8],
                         label='colorbar')
    return ax_cb, ax_psd_HF, ax_psd_LF, ax_psd_press, ax_seis_HF, ax_seis_LF,\
        ax_seis_press, ax_spec_HF, ax_spec_LF, ax_spec_press


def __dayplot_set_x_ticks(ax, starttime, endtime, sol=False):
    """
    Sets the xticks for the dayplot.
    """

    # day_break = endtime - float(endtime) % 86400
    # day_break -= float(day_break) % 1
    hour_ticks = []
    ticklabels = []
    interval = endtime - starttime
    interval_h = interval / 3600.
    ts = starttime
    tick_start = utct(ts.year, ts.month, ts.day, ts.hour)

    step = 86400
    if 0 < interval <= 60:
        step = 10
    elif 60 < interval <= 300:
        step = 30
    elif 300 < interval <= 900:
        step = 120
    elif 900 < interval <= 3600:
        step = 300
    elif 3600 < interval <= 18000:
        step = 1800
    elif 18000 < interval <= 43200:
        step = 3600
    elif 43200 < interval <= 86400:
        step = 4 * 3600
    elif 86400 < interval:
        step = 6 * 3600
    step_h = step / 3600.

    # make sure the start time is a multiple of the step
    if tick_start.hour % step_h > 0:
        tick_start += 3600 * (step_h - tick_start.hour % step_h)

    # for ihour in np.arange(0, interval_h + step_h * 2, step_h):
    for ihour in np.arange(0, interval_h + 2 + step_h, step_h):
        hour_tick = tick_start + ihour * 3600.
        hour_ticks.append(hour_tick)
        if sol:
            ticklabels.append(utct(hour_tick).strftime('%H:%M:%S%nSol %j'))
        else:
            ticklabels.append(utct(hour_tick).strftime('%H:%M:%S%n%Y-%m-%d'))

    hour_ticks_minor = []
    for ihour in np.arange(0, interval_h, 1):
        hour_tick = tick_start + ihour * 3600.
        hour_ticks_minor.append(hour_tick)

    ax.set_xlim(float(starttime),
                float(endtime))
    ax.set_xticks(hour_ticks)
    ax.set_xticks(hour_ticks_minor, minor=True)
    ax.set_xticklabels(ticklabels)
    ax.set_xlim(float(starttime),
                float(endtime))


def def_args():
    helptext = 'Calculate spectograms to quickly check content of hours ' \
               'to days of seismic data. '                                \
               'The code assumes that the input files are instrument  ' \
               'corrected to acceleration. ' \
               'If this is not the case, adapt the value range (dBmin, dBmax)'
    parser = argparse.ArgumentParser(helptext)
    parser.add_argument('--lp', nargs='+', required=True,
                        help='Data file(s) containing the long period data')
    parser.add_argument('--hf', nargs='+', required=True,
                        help='Data file(s) containing the high frequency data')
    parser.add_argument('--p', nargs='+', required=True,
                        help='Data file(s) containing the pressure time series')
    parser.add_argument('--dBmin', type=float, default=-180,
                        help='Minimum value for spectrograms (in dB)')
    parser.add_argument('--dBmax', type=float, default=-100,
                        help='Maximum value for spectrograms (in dB)')
    parser.add_argument('--tstart', default=None,
                        help='Start time for spectrogram' +
                             '(in any format that datetime understands)')
    parser.add_argument('--tend', default=None,
                        help='End time for spectrogram')
    parser.add_argument('--winlen', default=100., type=float,
                        help='Window length for long-period spectrogram (in '
                             'seconds)')
    parser.add_argument('--no_noise', default=False, action='store_true',
                        help='Omit plotting the NLNM/NHNM curves')
    parser.add_argument('--fmax', default=5.0, type=float,
                        help='Omit plotting the NLNM/NHNM curves')
    parser.add_argument('-i', '--interactive', default=False,
                        action='store_true',
                        help='Open a matplotlib window instead of saving to '
                             'disk')

    parser.add_argument('--kind', default='spec',
                        help='Calculate spectrogram (spec) or continuous '
                             'wavelet transfort (cwt, much slower)? Default: '
                             'spec')
    return parser.parse_args()


if __name__ == '__main__':
    args = def_args()

    starttime = None
    endtime = None
    if args.tstart:
        starttime = obspy.UTCDateTime(args.tstart)
    if args.tstart:
        endtime = obspy.UTCDateTime(args.tend)
    print(starttime)
    print(endtime)

    st_LF = obspy.Stream()
    for file in args.lp:
        st_LF += obspy.read(file, starttime=starttime - 120.,
                            endtime=endtime + 120.)
    st_HF = obspy.Stream()
    for file in args.hf:
        st_HF += obspy.read(file, starttime=starttime - 120.,
                            endtime=endtime + 120.)

    if st_LF[0].stats.sampling_rate > 5.0:
        dec_fac = int(st_LF[0].stats.sampling_rate / 5.0)
        st_LF.decimate(dec_fac)

    st_press = obspy.Stream()
    for file in args.p:
        st_press += obspy.read(file, starttime=starttime - 120.,
                               endtime=endtime + 120.)

    for st in [st_HF, st_LF]:
        st.detrend()
        st.filter('highpass', freq=1./args.winlen)
        st.trim(starttime=starttime,
                endtime=endtime)

    if args.interactive:
        fnam_out = None
    else:
        fnam_templ_1 = "spectrogram_{network}.{location}.{station}." \
                       "{channel}_vs_"
        fnam_templ_2 = "{network}.{location}.{station}.{channel}-" \
                       "{starttime.year}.{starttime.julday}.png"
        fnam_out = fnam_templ_1.format(**st_LF[0].stats) + \
            fnam_templ_2.format(**st_HF[0].stats)
    if args.no_noise:
        noise = None
    else:
        noise = 'Earth'
    calc_specgram_dual(st_LF, st_HF, st_press,
                       winlen_sec_LF=args.winlen,
                       winlen_sec_HF=10.,
                       vmin=args.dBmin,
                       vmax=args.dBmax,
                       fmax=args.fmax,
                       noise=noise,
                       fnam=fnam_out,
                       kind=args.kind)
