#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
An approximate time converter for UTC and LMST at the insight landing site.

:copyright:
    Simon Stähler (mail@simonstaehler.com), 2018
    Martin van Driel (Martin@vanDriel.de), 2018
:license:
    None
'''
from obspy import UTCDateTime

SEC_PER_DAY_EARTH = 86400
SEC_PER_DAY_MARS = 88775.244147


def solify(UTC_time, sol0=UTCDateTime(2018, 11, 26, 5, 9, 18.7),
           tai_utc=37):

    MIT = (UTC_time - sol0) / SEC_PER_DAY_MARS
    t = UTCDateTime((MIT - 1) * SEC_PER_DAY_EARTH)
    return t


def UTCify(LMST_time, sol0=UTCDateTime(2018, 11, 26, 5, 9, 18.7),
           tai_utc=37):

    MIT = float(LMST_time) / SEC_PER_DAY_EARTH + 1
    UTC_time = UTCDateTime(MIT * SEC_PER_DAY_MARS + float(sol0))
    return(UTC_time)


if __name__ == "__main__":
    for utc in [UTCDateTime('2018-12-09T10:39'),
                UTCDateTime('2018-12-13T13:01'),
                UTCDateTime('2019-12-13T13:01')]:
        print(utc)
        lmst = solify(utc)
        print(lmst)
        utc = UTCify(lmst)
        print(utc)
        print()
